#!/usr/bin/env python
# coding: utf-8

# # CHALLENGE ASSIGNMENT_DSSP

# ## Finding a Repo and Running it.

# Qirjako Panoti

# Matr.Nr:429826

# ## Repo link on Github

# The link of the repo is given below:

# https://github.com/reclosedev/pyautocad

# Description: This is a python library which makes possible drawing of elements in an Autocad file.
# An Autocad file must be opened first and after running the script the elements are added in the Cad model.
# It can be used with a blank file or it can add extra elements in an existing drawing.

# # Installation

# Created an environment in Anaconda named "CAD_PYTHON"

# pip install --upgrade pyautocad

# Check if library is installed:
# Pip3 show pyautocad           

# Open an Autocad file

# # Testing the script

# The default script found in the repository is as follows:

# In[1]:


from pyautocad import Autocad, APoint


# In[2]:


acad = Autocad()


# In[3]:


acad.prompt("Hello, Autocad from Python\n")


# In[4]:


print (acad.doc.Name)


# In[5]:


p1 = APoint(0, 0)


# In[6]:


p2 = APoint(50, 25)


# In[7]:


for i in range(5):
    text = acad.model.AddText('Hi %s!' % i, p1, 2.5)
    acad.model.AddLine(p1, p2)
    acad.model.AddCircle(p1, 10)
    p1.y += 10       


# In[8]:


dp = APoint(10, 0)


# In[9]:


for text in acad.iter_objects('Text'):
    print('text: %s at: %s' % (text.TextString, text.InsertionPoint))
    text.InsertionPoint = APoint(text.InsertionPoint) + dp


# In[10]:


for obj in acad.iter_objects(['Circle', 'Line']):
    print(obj.ObjectName)


# The Cad output should display a drawings as in the following picture:

# ![default-2.PNG](attachment:default-2.PNG)
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 

# In[ ]:





# # Modifying the script

# I modified the script by adding some extra circles in the drawing

# The circles will have the same center (concentric circles) and various diameter.

# The diameter proportions between each circle follow the rule of Fibonacci sequence starting with a circe with radius-10

# As a procedure I used a fibonacci generator in python and then I added the circle function inside a while loop.

# The origin is the global 0,0 in Cad model

# ##   Define the origin
# 

# In[11]:


p1 = APoint(0, 0)


# ##   The Fibonacci Sequence
# 

# In[12]:


a = 0


# In[13]:


b = 10


# In[14]:


c = 0


# In[15]:


i = 1


# In[16]:


print(a)


# In[17]:


print(b)


# In[18]:


while i < 100:
    c = a+b  
    radius = int(c)
    print("radius = ",radius)               
    acad.model.AddCircle(p1, (radius))
    
    a = b    
    b = c    
    i = i + 10 


# In[19]:


print("----------------PLEASE SEE THE CAD DRAWING-------------------")


# The drawing should look like the picture below:

# ![output.PNG](attachment:output.PNG)
# 
# 
# 

# # Setup the right kernel for Jupyter Notebook

# At the base environment I installed the kernels:
# Conda install nb_conda_kernels

# At my specific environment ("CAD_PYTHON" environment) I installed ipykernel

# Then opened the Jupyter Notebook and I chose the kernel of my specific environment ("CAD_PYTHON" environment)
